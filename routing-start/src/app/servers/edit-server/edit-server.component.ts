import { Component, OnInit } from '@angular/core';

import { ServersService } from '../servers.service';
import {Router, ActivatedRoute, Params} from '@angular/router'
@Component({
  selector: 'app-edit-server',
  templateUrl: './edit-server.component.html',
  styleUrls: ['./edit-server.component.css']
})
export class EditServerComponent implements OnInit {
  server: {id: number, name: string, status: string};
  serverName = '';
  serverStatus = '';
  allowEdit = false;
  constructor(private serversService: ServersService,
   private route:ActivatedRoute) { }

  ngOnInit() {

    this.route.queryParams.subscribe(
     (queryParams:Params)=> {
       this.allowEdit = queryParams['allowEdit'] === '1' ? true:false ;
     }); //get the queryparams and fragments if they change
    const id = +this.route.snapshot.params[+'id'];
    this.route.fragment.subscribe();
    this.server = this.serversService.getServer(id);
    this.route.params
      .subscribe(
       (params:Params)=>{
         this.server = this.serversService.getServer(+params['id']);
       }
      );
    this.serverName = this.server.name;
    this.serverStatus = this.server.status;
  }

  onUpdateServer() {
    this.serversService.updateServer(this.server.id, {name: this.serverName, status: this.serverStatus});
  }

}
